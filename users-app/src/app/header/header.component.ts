import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../login/auth.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {

  title = 'App Users';

  constructor(private authService: AuthService,
    private router: Router) { }

  logout(): void {
    this.authService.logout();
    swal.fire('Logout', `You're logged out now!`, 'success');
    this.router.navigate(['/login']);
  }

}
