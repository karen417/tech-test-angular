import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../users/user';
import { AuthService } from './auth.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  title: string = "Please, Sign In!";
  user: User;

  constructor(private authService: AuthService, private router: Router) {
    this.user = new User();
  }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      swal.fire('Login', `Hi ${this.authService.user.username}, you're already logged in!`, 'info');
      this.router.navigate(['/users']);
    }
  }

  login(): void {
    if (this.user.username == null || this.user.password == null) {
      swal.fire('Error Login', 'Empty username or password!', 'error');
      return;
    }

    this.authService.login(this.user).subscribe(
      response => {
        this.authService.setSessionUser(response.access_token);
        this.authService.setSessionToken(response.access_token);
        let user = this.authService.user;
        this.router.navigate(['/users']);
        swal.fire('Login', `Hi ${user.username}, you're now logged in!`, 'success');
      },
      err => {
        if (err.status = 400) {
          swal.fire('Error Login', 'Incorrect username or password!', 'error');
        }
      }
    )

  }

}
