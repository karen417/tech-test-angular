import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

import { AuthService } from 'src/app/login/auth.service';

import swal from 'sweetalert2';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,
    private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError(e => {
        if (e.status == 401) {
          if (this.authService.isAuthenticated()) {
            this.authService.logout();
          }
          swal.fire('Not Authenticated', 'Please, sign in to view this content!', 'warning');
          this.router.navigate(['/login']);
        }
        if (e.status == 403) {
          swal.fire('Access denied', 'You are not authorized to view this content!', 'warning');
          this.router.navigate(['/users']);
        }
        return throwError(e);
      })
    );
  }
}
