import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlEndPoint: string = 'http://localhost:8080/api/users/';

  constructor(private http: HttpClient,
    private router: Router) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.urlEndPoint).pipe(
      catchError(e => {
        if (e.error.message) {
          console.error(e.error.message);
        }
        return throwError(e);
      })
    );
  }

  getUser(id): Observable<User> {
    return this.http.get<User>(this.urlEndPoint+id).pipe(
      catchError(e => {
        if (e.status != 401 && e.error.message) {
          this.router.navigate(['/users']);
          console.error(e.error.message);
        }
        return throwError(e);
      })
    );
  }

  create(user: User): Observable<User> {
    return this.http.post(this.urlEndPoint, user).pipe(
      map((response: any) => response.user as User),
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.message) {
          console.error(e.error.message);
        }
        return throwError(e);
      })
    );
  }

  update(user: User): Observable<any> {
    return this.http.put<any>(this.urlEndPoint+user.id, user).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.message) {
          console.error(e.error.message);
        }
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<User> {
    return this.http.delete<User>(this.urlEndPoint+id).pipe(
      catchError(e => {
        if (e.error.message) {
          console.error(e.error.message);
        }
        return throwError(e);
      })
    );
  }
}
