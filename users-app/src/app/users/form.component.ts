import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { User } from './user';
import { Role } from '../roles/role';
import { RoleService } from '../roles/role.service';
import { UserService } from './user.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private user: User = new User();

  rolesControl = new FormControl();
  roles: Role[];
  private errs: string[];

  constructor(private userService: UserService,
    private roleService: RoleService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      if (id) {
        this.userService.getUser(id)
          .subscribe(user => this.user = user)
      }
    });

    this.roleService.getRoles().pipe()
      .subscribe((response: any) => {
        this.roles = response as Role[];
      });
  }

  create(): void {
    this.userService.create(this.user)
      .subscribe(
        user => {
          this.router.navigate(['/users'])
          swal.fire('New user', `User ${this.user.username} has been successfully created!`, 'success')
        },
        err => {
          this.errs = err.error.errors as string[];
          console.error(err.error.errors);
        }
      );
  }

  update(): void {
    this.userService.update(this.user)
      .subscribe(
        json => {
          this.router.navigate(['/users'])
          swal.fire('User update', 'User successfully updated!', 'success')
        },
        err => {
          this.errs = err.error.errors as string[];
          console.error(err.error.errors);
        }
      );
  }

}
