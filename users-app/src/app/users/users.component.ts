import { Component, OnInit } from '@angular/core';
import { Sort } from '@angular/material';

import { User } from './user';
import { UserService } from './user.service';
import { AuthService } from '../login/auth.service';

import swal from 'sweetalert2';
import { Role } from '../roles/role';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {

  users: User[];
  sortedData: User[];
  selectedUser: User;

  constructor(private userService: UserService,
    private authService: AuthService) { }

  ngOnInit() {
    this.userService.getUsers().pipe()
      .subscribe((response: any) => {
        this.users = response as User[];
        this.sortedData = this.users.slice();
      });
  }

  sortData(sort: Sort) {
    const data = this.users.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id': return compare(a.id, b.id, isAsc);
        case 'name': return compare(a.name, b.name, isAsc);
        default: return 0;
      }
    });
  }

  delete(user: User): void {
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'Delete user',
      text: `User '${user.name}' will be deleted...`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Delete!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.userService.delete(user.id)
          .subscribe(response => {
            this.users = this.users.filter(u => u !== user);
            this.sortedData = this.users.slice();
            swalWithBootstrapButtons.fire(
              'Deleted!',
              `User '${user.name}' was successfully deleted.`,
              'success'
            );
          });
      }
    });
  }

  showRoles(roles: Role[]): void {
    let rolesString = '';
    for (let role of roles) {
      rolesString = rolesString + (role.name.slice(5).charAt(0).toUpperCase() + role.name.slice(6).toLowerCase() + '\n');
    }
    swal.fire({
      title: 'Roles',
      html:
        `<div class='multi_lines_text'>${rolesString}</div><style>.multi_lines_text { white-space: pre-line; }</style>`,
      showCloseButton: true,
      confirmButtonText:
        'Close',

    })
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
